To run the server (assuming python refers to a python3 installation, of version 3.5 or greater)

1. Setup a virtual enviornment: `python -m venv env`
2. Active it: `source env/bin/activate`
3: Install the requirements: `pip install -r requirements.txt`
4. Run the server: `python src/api.py`
5. To test you can do `curl -X GET localhost:8080/notifications/b1638f970c3ddd528671df76c4dcf13e`

To test, again having setup and activated the virtual environment you can do:

`pytest tests/test.py`
