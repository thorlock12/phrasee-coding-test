import pytest
from posts import PostStore, PostNotFoundError

POST_ID = "b1638f970c3ddd528671df76c4dcf13e"
NAMES = ["Bojana Novaković", "Mary T. Price",  "Ali Sage"]
COMMENT_IDS = [
    "e1788fb8b05d79c793c2002d57e80182",
    "410065f8dee7354ba6fa7f5552092657",
    "46f72ffb3a5717dcd71e26369d1e13a5",
]

DATA = [{
    "type": "Like",
    "post": {
        "id": POST_ID,
        "title": "Acme Inc dynamically scales niches worldwide"
    },
    "user": {
        "id": "403f220c3d413fe9cb0b36142ebfb35d",
        "name": NAMES[0]
    }
}, {
    "type": "Like",
    "post": {
        "id": POST_ID,
        "title": "Acme Inc dynamically scales niches worldwide"
    },
    "user": {
        "id": "cc81a8b1fceb3306997be05426b668e4",
        "name": NAMES[1]
    }
}, {
    "type": "Like",
    "post": {
        "id": POST_ID,
        "title": "Acme Inc dynamically scales niches worldwide"
    },
    "user": {
        "id": "7305d0a8bb9d7166b8d26ca856930b8d",
        "name": NAMES[2]
    }
}, {
    "type": "Comment",
    "post": {
        "id": POST_ID,
        "title": "Acme Inc dynamically scales niches worldwide"
    },
    "comment":{
        "id": COMMENT_IDS[0],
        "commentText": "😩 I really disagree with the content of this post. I belive the the client should really try to synthesize internal or 'organic' sources before not after leveraging!"
    },
    "user":{
        "id": "f1333326efc51be3d620d80f72c55944",
        "name": "Harold Lachlan"
    }
}, {
    "type": "Comment",
    "post": {
        "id": POST_ID,
        "title": "Acme Inc dynamically scales niches worldwide"
    },
    "comment":{
        "id": COMMENT_IDS[1],
        "commentText": "😂 That'll be the day!"
    },
    "user":{
        "id": "38bbd19a89ac307cff5ab2b5bf83783a",
        "name": "Chuck Looij"
    }
}, {
    "type": "Comment",
    "post": {
        "id": POST_ID,
        "title": "Acme Inc dynamically scales niches worldwide"
    },
    "comment":{
        "id": COMMENT_IDS[2],
        "commentText": "Acme remains one of my fave company ever! The way they scale is so dynamic that makes HTML5 look static!"
    },
    "user":{
        "id": "5497afbf9df3f6ff6f9ba11cdef5310f",
        "name": "Suoma Narjus"
    }
}]

def test_ensure_user():
    post_store = PostStore()
    post_store.ensure_user({"user": {"id": 1, "name": "a"}})
    assert  post_store._username_cache[1] == "a"


def test_read_notifications_likes():
    post_store = PostStore()
    post_store.read_notifications(DATA)
    notifications = post_store.get_notifications(POST_ID)
    for name in NAMES[:2]:
        assert name in notifications["likes"]

    assert name[2] not in notifications["likes"]
    assert notifications["total_likes"] == 3
    assert notifications["other_likes"] == 1


def test_notifications_comments():
    post_store = PostStore()
    post_store.read_notifications(DATA)
    notifications = post_store.get_notifications(POST_ID)
    assert len(notifications["comments"]) == 2
    for i, comment_id in enumerate(COMMENT_IDS[:2]):
        assert comment_id == notifications["comments"][i]["id"]

    assert notifications["total_comments"] == 3
    assert notifications["other_comments"] == 1


def test_post_not_found():
    post_store = PostStore()
    post_store.read_notifications([])
    with pytest.raises(PostNotFoundError):
        post_store.get_notifications("a")


def test_max_type_1():
    post_store = PostStore()
    post_store.read_notifications(DATA)
    post = post_store.get_post(POST_ID)
    notifications = post.get_notifications(type_max=1)

    assert len(notifications["comments"]) == 1
    assert len(notifications["likes"]) == 1

    assert notifications["total_comments"] == 3
    assert notifications["other_comments"] == 2
    assert notifications["total_likes"] == 3
    assert notifications["other_likes"] == 2
