### Backend technical test

You are tasked with writing the algorithm that takes a feed of notifications and aggregates them. The algorithm should be packaged in a web server that exposes one endpoint. The endpoint will provide a GET method and accept the ID of a post and return all the aggregated notifications for that post. We are looking for a response that is as close to production ready as possible.

The notification feed is from a hypothetical social website that allows users to write posts, like posts and comment on posts. The notifications can be of two types: Like and Comment. Like indicates that one user liked a user's post and Comment indicates that one user commented on a user's post.


The notifications should be aggregated per type and post, you'll be provided with a file containing a JSON of the notifications feed and another file showing how the notifications are expected to be aggregated. Please note that the order in which the notifications are served or aggregated is irrelevant.
