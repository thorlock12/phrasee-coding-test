from collections import namedtuple

Like = namedtuple("Like", "user_id")
Comment = namedtuple("Comment", "comment_id user_id text")


class PostNotFoundError(Exception):
    def __init__(self, post_id):
        self.post_id = post_id

    def __str__(self):
        return "{} not found!".format(self.post_id)


class Post(object):
    def __init__(self, post, username_cache):
        self.post_id = post["id"]
        self.title = post["title"]

        self._username_cache = username_cache
        self._comments = []
        self._likes = []

    def add_comment(self, comment):
        self._comments.append(comment)

    def add_like(self, like):
        self._likes.append(like)

    def get_notifications(self, type_max=2):
        """
        Gets comments and likes, up to a maximum of `type_max` of each.
        """
        return {
            "likes": [
                self._username_cache[l.user_id] for l in self._likes[:type_max]
            ],
            "comments": [{
                "id": c.comment_id,
                "user": self._username_cache.get(c.user_id),
                "text": c.text,
            } for c in self._comments[:type_max]],
            "total_likes": len(self._likes),
            "total_comments": len(self._comments),
            "other_likes": max(0, len(self._likes) - type_max),
            "other_comments": max(0, len(self._comments) - type_max),
            "title": self.title,
            "id": self.post_id,
        }


class PostStore(object):
    def __init__(self):
        # post_id -> Post
        self._posts = {}

        # user_id -> user_name
        self._username_cache = {}

    def ensure_user(self, notification):
        self._username_cache[notification["user"]["id"]] = notification["user"]["name"]

    def read_notifications(self, data):
        for notification in data:
            self.ensure_user(notification)

            if notification["post"]["id"] in self._posts:
                post = self._posts[notification["post"]["id"]]
            else:
                post = Post(notification["post"], self._username_cache)
                self._posts[notification["post"]["id"]] = post

            if notification["type"] == "Like":
                like = Like(user_id=notification["user"]["id"])
                post.add_like(like)
            elif notification["type"] == "Comment":
                comment = Comment(
                    comment_id=notification["comment"]["id"],
                    user_id=notification["user"]["id"],
                    text=notification["comment"]["commentText"]
                )
                post.add_comment(comment)

    def get_post(self, post_id):
        try:
            return self._posts[post_id]
        except KeyError:
            raise PostNotFoundError(post_id)

    def get_notifications(self, post_id):
        return self.get_post(post_id).get_notifications()
