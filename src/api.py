import json
from aiohttp import web
from posts import PostStore, PostNotFoundError


class Server(object):
    def __init__(self, post_store):
        self.post_store = post_store

    async def get_notifications(self, request):
        post_id = request.match_info["post_id"]
        try:
            notifications = self.post_store.get_notifications(post_id)
        except PostNotFoundError as e:
            return web.json_response({"error": str(e)}, status=404)
        else:
            return web.json_response(notifications)

    @property
    def routes(self):
        return [
            web.get("/notifications/{post_id}", self.get_notifications)
        ]

    def run(self):
        middlewares = (web.normalize_path_middleware(
            append_slash=True, merge_slashes=True
        ),)
        app = web.Application(middlewares=middlewares)
        app.add_routes(self.routes)
        web.run_app(app)


if __name__ == "__main__":
    with open("notifications-feed.json", "r") as f:
        data = json.loads(f.read())

    post_store = PostStore()
    post_store.read_notifications(data)

    server = Server(post_store)
    server.run()
